# monitor-lock

Lock a cursor to a single monitor using
[XFIXES 5.0 barriers](https://who-t.blogspot.com/2012/12/whats-new-in-xi-23-pointer-barrier.html)
to stop the cursor from moving off the specified screen. If the cursor
is not presently on that screen, then it can be moved onto it but not
back off of it.

The intention is for this to be used in conjunction with the
[multi-pointer X](https://wiki.archlinux.org/index.php/Multi-pointer_X)
support of XInput2 to create a multi-seat setup where different pointers
are assigned to different monitors, but it may have other uses.

Run it once without arguments to see the options for devices and screens.
For example, the output may look like this:
```sh
$ ./monitor-lock.py 
Found XFIXES version 5.0
Found XInputExtension version 2.0
Found XINERAMA version 1.1

Available screens:
screen 0: {'x': 0, 'y': 0, 'width': 3840, 'height': 2160}
screen 1: {'x': 3840, 'y': 0, 'width': 1920, 'height': 1200}
screen 2: {'x': 3840, 'y': 1200, 'width': 1920, 'height': 1080}

Available pointers:
device 2: Virtual core pointer

USAGE: ./monitor-lock.py [device] [screen]
```

Then running
```sh
./monitor-lock.py 2 0
```
would keep the (in this example, only) cursor on the 3840x2160 monitor
for as long as `monitor-lock.py` was still running.
