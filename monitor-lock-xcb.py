#!/usr/bin/env python3
import sys
import os
import time

import xcffib, xcffib.xproto
import xcffib.xfixes
from xcffib.xfixes import BarrierDirections
import xcffib.xinerama
import xcffib.xinput

def main(argv):
    # XCB boilerplate stuff:
    x11_display = os.environ['DISPLAY']
    conn = xcffib.connect(display=x11_display)
    setup = conn.get_setup()
    screen = setup.roots[0]
    width = screen.width_in_pixels
    height = screen.height_in_pixels
    root = screen.root

    conn.xfixes = conn(xcffib.xfixes.key)
    conn.xinerama = conn(xcffib.xinerama.key)
    conn.xinput = conn(xcffib.xinput.key)

    conn.xfixes.QueryVersion(5, 0, True).reply()

    print()
    print('Available screens:')
    screens = conn.xinerama.QueryScreens(root).reply().screen_info
    for (idx, screen) in enumerate(screens):
        x, y, w, h = screen.x_org, screen.y_org, screen.width, screen.height
        print('screen %d: pos=%s, size=%s' % (idx, (x, y), (w, h)))

    print()
    print('Available pointers:')
    pointer_name = {}
    devices = conn.xinput.XIQueryDevice(xcffib.xinput.Device.AllMaster).reply().infos
    for device in devices:
        name = device.name.to_string()
        if 'pointer' in name:
            pointer_name[device.deviceid] = 'device %d: %s' % (device.deviceid, name)
            print(pointer_name[device.deviceid])

    if len(sys.argv) != 3:
        print()
        print('USAGE: %s [device] [screen]' % (sys.argv[0]))
        return -2
    lock_device = int(sys.argv[1])
    lock_screen = int(sys.argv[2])

    screen = screens[lock_screen]

    print()
    print('Creating barrier to force pointer %s to screen %d (%s)...' %\
            (pointer_name[lock_device], lock_screen, (screen.x_org, screen.y_org, screen.width, screen.height)))
    top = screen.y_org + 3
    bottom = screen.y_org + screen.height - 3
    left = screen.x_org + 1
    right = screen.x_org + screen.width - 1
    print('Barrier bounds: left=%d, top=%d, right=%d, bottom=%d' % (left, top, right, bottom))
    # top
    conn.xfixes.CreatePointerBarrier(conn.generate_id(), root, left, top, right, top, BarrierDirections.PositiveY, 1, [lock_device], True).check()
    # left
    conn.xfixes.CreatePointerBarrier(conn.generate_id(), root, left, top, left, bottom, BarrierDirections.PositiveX, 1, [lock_device], True).check()
    # bottom
    conn.xfixes.CreatePointerBarrier(conn.generate_id(), root, left, bottom, right, bottom, BarrierDirections.NegativeY, 1, [lock_device], True).check()
    # right
    conn.xfixes.CreatePointerBarrier(conn.generate_id(), root, right, top, right, bottom, BarrierDirections.NegativeX, 1, [lock_device], True).check()
    conn.flush()
    print('Created barrier.')
    input()

if __name__ == '__main__':
    sys.exit(main(sys.argv))
